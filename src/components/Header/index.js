import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Button, Title,Icon, Content,ListItem,List} from 'native-base';
import {View, Text, StyleSheet, TouchableOpacity, Alert} from "react-native";
import {NavigationActions,DrawerActions} from 'react-navigation-drawer';
import {responsiveFontSize,responsiveHeight,responsiveWidth} from 'react-native-responsive-dimensions';
import ModalDropdown from 'react-native-modal-dropdown';
import Modal from "../optionModal";

export default class HeaderCOM extends Component {
  constructor(props){
    super(props);
    this.state={
      showmodal:false,
    }
}
option = () => {
  this.setState({showmodal:true})
 

}
  render() {
    //console.log(this.props);
    return (
      <>
        <Header style={{backgroundColor:"#000046"}} >
          <Left >
            {this.props.ltbtnValue &&
            <TouchableOpacity onPress={()=> this.props.onActionLtBtn()}>
              <Icon name={this.props.ltbtnTitle} style={{color:"#fff",fontSize:responsiveFontSize(4)}}/>
            </TouchableOpacity>
          }
          </Left>
          <Body>
            <Text style={{color:"#fff",fontSize:responsiveFontSize(2.5)}}>{this.props.name}</Text>
          </Body>
          <Right>
          {this.props.rtfBtnValue &&
            <TouchableOpacity onPress={()=>this.Modal.open()}>
            <Icon name={this.props.rtbtnTitle}  type="Entypo" style={{color:"#fff",fontSize:responsiveFontSize(3)}}/>
            </TouchableOpacity>
          }
         
            
         </Right>
        
        </Header>
        <Modal
                title="Home"
                onRef = {ref => this.Modal = ref}
                Description = "I am from Home screen"
                okbtn = {true}
                cnlbtn = {true}
                onActionLft = {()=>this.props.navigation.navigate('setting')}
                onActionRgt = {()=>this.props.navigation.navigate('support')}
            />
          
         </>
    );
  }
}