import React, { Component } from 'react';
import { Container, Header, Content, Footer, FooterTab, Button, Icon, Badge } from 'native-base';
import {View, Text, StyleSheet, TouchableOpacity,Image, ImageBackground} from "react-native";

export default class FooterTabsBadgeExample extends Component {
  render() {
    return (
      <Container>
    
        <Content />
        <Footer >
        <FooterTab style={{backgroundColor:"#075E54"}}>
        <Button >
              <Icon name="home" style={{color:"#fff"}} />
            </Button>
            <Button >
              <Icon name="search" style={{color:"#fff"}}/>
            </Button>
            <Button >
              <Icon name="plus-circle-outline" type="MaterialCommunityIcons" style={{color:"#fff"}}/>
            </Button>
            <Button >
              <Icon name="chat" type="MaterialCommunityIcons" style={{color:"#fff"}}/>
            </Button>
            <Button >
              <Icon name="user-circle" type="FontAwesome5" style={{color:"#fff"}} />
            </Button>
          </FooterTab>
          
        </Footer>
      </Container>
    );
  }
}