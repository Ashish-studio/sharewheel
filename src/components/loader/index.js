import React , {Component} from "react";
import {Text, View, StyleSheet, ActivityIndicator,Modal} from "react-native";
import { Container, Header, Content, Card, CardItem, Body, Input,Item,Icon,Label,Button,Fab} from "native-base";
import { responsiveHeight, responsiveWidth, responsiveFontSize } from "react-native-responsive-dimensions";
import { TouchableOpacity } from "react-native-gesture-handler";

export default class Loader extends Component{
    constructor(props){
        super(props);
        this.state = {
            isOpen: true,
        }
    }
    componentDidMount(){
        this.props.onRef(this.refs.Modal); 
    }

    render(){
        return (
            <Modal
                animationType="fade"
                visible={this.state.isOpen}
                ref = {"Modal"}
                
            >
                <Container>
                   <View style={{flex:1,alignItems:"center",justifyContent:"center",backgroundColor:"grey"}}>
                       <ActivityIndicator color="white">

                       </ActivityIndicator>
                   </View>

                </Container>
            </Modal>
           
        );
    }
}

const styles = StyleSheet.create({
    container:{
        justifyContent:"center",
        flex:1,
        alignItems:"center",
        backgroundColor:"red"
    },
    modal: {
        backgroundColor:"#f2f2f2",
        height:responsiveHeight(100),
        width:responsiveWidth(100)
    },
    btn: {
        margin: 10,
        backgroundColor: "#3B5998",
        color: "white",
        padding: 10
    },
    btnModal: {
        position: "absolute",
        top: 0,
        right: 0,
        width: 50,
        height: 50,
        backgroundColor: "transparent"
    },
    
    text: {
        color: "black",
        fontSize: 22
    }
});