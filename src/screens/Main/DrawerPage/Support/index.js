import React from 'react';
import { StyleSheet, Text, View,TouchableOpacity,ScrollView,ImageBackground,Image } from 'react-native';
import {Header,Left,Icon} from "native-base";
import {responsiveHeight,responsiveFontSize,responsiveWidth} from "react-native-responsive-dimensions";

export default class Support extends React.Component {
    
    render(){
        return (
                        <View style={{flex:1}}>
                        <View  style={{flex:0.1,flexDirection:"row", backgroundColor:'#000046'}}>
                            <TouchableOpacity onPress={()=>{this.props.navigation.goBack()}}>
                                <Icon name="arrow-back" style={{color:"white",marginTop:responsiveHeight(2)}}></Icon>
                            </TouchableOpacity>
                           <Text  style={{marginTop:responsiveHeight(2),color:"white",fontSize:responsiveFontSize(2.5)}}>   Help</Text>
                        </View>
          
            <View style={{flex:0.3,flexDirection:"row",margin:responsiveWidth(10)}}>
              <TouchableOpacity>
                <Image source={require("../../../../assets/icon/driver.png")} style={{borderWidth:2,borderColor:"black",width:responsiveWidth(20), height:responsiveHeight(15), borderRadius:5}}>
                </Image>
                <Text>About Driver</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{marginLeft:responsiveWidth(5)}}>
                <Image source={require("../../../../assets/icon/ride1.png")} style={{borderWidth:2,borderColor:"black",width:responsiveWidth(20), height:responsiveHeight(15), borderRadius:5}}>
                </Image>
                <Text>About Ride</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{marginLeft:responsiveWidth(5)}}>
                <Image source={require("../../../../assets/icon/payment.png")} style={{borderWidth:2,borderColor:"black",width:responsiveWidth(20), height:responsiveHeight(15), borderRadius:5}}>
                </Image>
                <Text>About Payment</Text>
              </TouchableOpacity>
            </View>
            <View style={{flex:0.3,flexDirection:"row",margin:responsiveWidth(10)}}>
              <TouchableOpacity>
                <Image source={require("../../../../assets/icon/referal.png")} style={{borderWidth:2,borderColor:"black",width:responsiveWidth(20), height:responsiveHeight(15), borderRadius:5}}>
                </Image>
                <Text>About Refer</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{marginLeft:responsiveWidth(5)}}>
                <Image source={require("../../../../assets/icon/safety.png")} style={{borderWidth:2,borderColor:"black",width:responsiveWidth(20), height:responsiveHeight(15), borderRadius:5}}>
                </Image>
                <Text>About Safety</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{marginLeft:responsiveWidth(5)}}>
                <Image source={require("../../../../assets/icon/any.png")} style={{borderWidth:2,borderColor:"black",width:responsiveWidth(20), height:responsiveHeight(15), borderRadius:5}}>
                </Image>
                <Text>Any Other</Text>
              </TouchableOpacity>
            </View>
          </View>
              
                
                        


     
        );
    }
}
