import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Body,  Icon,Item,Input,Button,Textarea, Left, Thumbnail, Right} from 'native-base';
import { TextInput ,View ,Text,StyleSheet,TouchableOpacity,Alert} from 'react-native';
import {CheckBox} from 'react-native-elements';
import {responsiveHeight,responsiveWidth, responsiveFontSize} from'react-native-responsive-dimensions'
import HeaderCOM from '../../../../components/Header';
import { LinearGradient } from 'expo-linear-gradient';

export default class Payment extends React.Component{
    constructor(props){
        super(props);
        this.state = {
                      check:true
                    };
        }   
render(){
return(
    <Container>
            <LinearGradient start={{x: 0, y: 0}} end={{x: 0, y: 1}} colors={['#1CB5E0', '#000046']}
                            style={{flex:1}}>
                <HeaderCOM {...this.props} 
                        name="Payments" 
                        ltbtnValue={true} 
                        rtfBtnValue={true} 
                        ltbtnTitle="arrow-back" 
                        rtbtnTitle="dots-three-vertical" 
                        onActionLtBtn = {()=>{this.props.navigation.goBack()}}
                        />
                <Content padder>
                    <Card style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                        <CardItem style={{backgroundColor:"#d9d9d9",borderRadius:responsiveWidth(5)}}>
                            <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5)}}>wallets</Text>
                        </CardItem>
                        <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                            <Left>
                                <Thumbnail source={require("../../../../assets/car.jpg")} 
                                        style={{height:responsiveWidth(10),width:responsiveWidth(10)}}/>
                                <Text style={{fontWeight:"400",marginLeft:responsiveWidth(3)}}>ShareWheelz Wallet</Text>
                            </Left>
                            <Right>
                                <Text>₹ 0.00</Text>
                            </Right>
                        </CardItem>
                        <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                            <Left>
                                <Thumbnail source={require("../../../../assets/icon/paytm.jpg")} 
                                        style={{height:responsiveWidth(10),width:responsiveWidth(10)}}/>
                                <Text style={{fontWeight:"400",marginLeft:responsiveWidth(3)}}>Paytm</Text>
                            </Left>
                            <Right>
                                <TouchableOpacity  onPress={()=>{Alert.alert("link paytm account")}}
                                                style={{backgroundColor:"#1CB5E0",height:responsiveHeight(4),width:responsiveWidth(15),
                                                    borderRadius:responsiveWidth(10),alignItems:"center",justifyContent:"center"}}>
                                    <Text style={{color:"#fff"}}>Link</Text>
                                </TouchableOpacity>
                            </Right>
                        </CardItem>
                        <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                            <Left>
                                <Thumbnail source={require("../../../../assets/icon/freecharge.png")} 
                                        style={{height:responsiveWidth(10),width:responsiveWidth(10)}}/>
                                <Text style={{fontWeight:"400",marginLeft:responsiveWidth(3)}}>FreeCharge</Text>
                            </Left>
                            <Right>
                                <TouchableOpacity  onPress={()=>{Alert.alert("link FreeCharge account")}}
                                                style={{backgroundColor:"#1CB5E0",height:responsiveHeight(4),width:responsiveWidth(15),
                                                    borderRadius:responsiveWidth(10),alignItems:"center",justifyContent:"center"}}>
                                    <Text style={{color:"#fff"}}>Link</Text>
                                </TouchableOpacity>
                            </Right>
                        </CardItem>  
                        <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                            <Left>
                                <Thumbnail source={require("../../../../assets/icon/paypal.jpg")} 
                                        style={{height:responsiveWidth(10),width:responsiveWidth(10)}}/>
                                <Text style={{fontWeight:"400",marginLeft:responsiveWidth(3)}}>PayPal</Text>
                            </Left>
                            <Right>
                                <TouchableOpacity  onPress={()=>{Alert.alert("link paypal account")}}
                                                style={{backgroundColor:"#1CB5E0",height:responsiveHeight(4),width:responsiveWidth(15),
                                                    borderRadius:responsiveWidth(10),alignItems:"center",justifyContent:"center"}}>
                                    <Text style={{color:"#fff"}}>Link</Text>
                                </TouchableOpacity>
                            </Right>
                        </CardItem>
                        <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                            <Left>
                                <Thumbnail source={require("../../../../assets/icon/amozonpay.jpg")} 
                                        style={{height:responsiveWidth(10),width:responsiveWidth(10)}}/>
                                <Text style={{fontWeight:"400",marginLeft:responsiveWidth(3)}}>Amazon Pay</Text>
                            </Left>
                            <Right>
                                <TouchableOpacity  onPress={()=>{Alert.alert("link amazonpay account")}}
                                                style={{backgroundColor:"#1CB5E0",height:responsiveHeight(4),width:responsiveWidth(15),
                                                    borderRadius:responsiveWidth(10),alignItems:"center",justifyContent:"center"}}>
                                    <Text style={{color:"#fff"}}>Link</Text>
                                </TouchableOpacity>
                            </Right>
                        </CardItem> 
                    </Card>
                    <Text/>
                    <Card style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                        <CardItem style={{backgroundColor:"#d9d9d9",borderRadius:responsiveWidth(5)}}>
                            <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5)}}>UPI's</Text>
                        </CardItem>
                        <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                            <Left>
                                <Thumbnail source={require("../../../../assets/icon/googlepay.png")} 
                                        style={{height:responsiveWidth(10),width:responsiveWidth(10)}}/>
                                <Text style={{fontWeight:"400",marginLeft:responsiveWidth(3)}}>GooglePay</Text>
                            </Left>
                            <Right>
                                <TouchableOpacity  onPress={()=>{Alert.alert("link gpay account")}}
                                                style={{backgroundColor:"#1CB5E0",height:responsiveHeight(4),width:responsiveWidth(15),
                                                    borderRadius:responsiveWidth(10),alignItems:"center",justifyContent:"center"}}>
                                    <Text style={{color:"#fff"}}>Link</Text>
                                </TouchableOpacity>
                            </Right>
                        </CardItem>
                        <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                            <Left>
                                <Thumbnail source={require("../../../../assets/icon/phonepe.jpg")} 
                                        style={{height:responsiveWidth(10),width:responsiveWidth(10)}}/>
                                <Text style={{fontWeight:"400",marginLeft:responsiveWidth(3)}}>PhonePe</Text>
                            </Left>
                            <Right>
                                <TouchableOpacity  onPress={()=>{Alert.alert("link phonepe account")}}
                                                style={{backgroundColor:"#1CB5E0",height:responsiveHeight(4),width:responsiveWidth(15),
                                                    borderRadius:responsiveWidth(10),alignItems:"center",justifyContent:"center"}}>
                                    <Text style={{color:"#fff"}}>Link</Text>
                                </TouchableOpacity>
                            </Right>
                        </CardItem>  
                        <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                            <Left>
                                <Thumbnail source={require("../../../../assets/icon/bhim.png")} 
                                        style={{height:responsiveWidth(10),width:responsiveWidth(10)}}/>
                                <Text style={{fontWeight:"400",marginLeft:responsiveWidth(3)}}>Bhim</Text>
                            </Left>
                            <Right>
                                <TouchableOpacity  onPress={()=>{Alert.alert("link bhim account")}}
                                                style={{backgroundColor:"#1CB5E0",height:responsiveHeight(4),width:responsiveWidth(15),
                                                    borderRadius:responsiveWidth(10),alignItems:"center",justifyContent:"center"}}>
                                    <Text style={{color:"#fff"}}>Link</Text>
                                </TouchableOpacity>
                            </Right>
                        </CardItem>
                        <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                            <Left>
                                <Thumbnail source={require("../../../../assets/icon/paytmbank.jpg")} 
                                        style={{height:responsiveWidth(10),width:responsiveWidth(10)}}/>
                                <Text style={{fontWeight:"400",marginLeft:responsiveWidth(3)}}>Paytm Payments Bank</Text>
                            </Left>
                            <Right>
                                <TouchableOpacity  onPress={()=>{Alert.alert("link paytm payments bank account")}}
                                                style={{backgroundColor:"#1CB5E0",height:responsiveHeight(4),width:responsiveWidth(15),
                                                    borderRadius:responsiveWidth(10),alignItems:"center",justifyContent:"center"}}>
                                    <Text style={{color:"#fff"}}>Link</Text>
                                </TouchableOpacity>
                            </Right>
                        </CardItem> 
                    </Card>
                    <Text/>
                    <Card style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                        <CardItem style={{backgroundColor:"#d9d9d9",borderRadius:responsiveWidth(5)}}>
                            <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5)}}>Online Payments</Text>
                        </CardItem>
                        <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                            <TouchableOpacity  onPress={()=>{Alert.alert("fill card details")}} style={{flexDirection:"row"}}>
                                <Left>
                                    <Thumbnail source={require("../../../../assets/icon/cards.jpg")} 
                                            style={{height:responsiveWidth(10),width:responsiveWidth(10)}}/>
                                    <Text style={{fontWeight:"400",marginLeft:responsiveWidth(3)}}>Credit,debit & ATM Cards</Text>
                                </Left>
                                <Right>
                                    <Icon name="ios-arrow-forward" type="Ionicons"/>
                                </Right>
                            </TouchableOpacity>
                        </CardItem>
                        <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                            <TouchableOpacity  onPress={()=>{Alert.alert(" open netbanking portel ")}} style={{flexDirection:"row"}}>
                                <Left>
                                    <Thumbnail source={require("../../../../assets/icon/onlinebankig.jpg")} 
                                            style={{height:responsiveWidth(10),width:responsiveWidth(10)}}/>
                                    <Text style={{fontWeight:"400",marginLeft:responsiveWidth(3)}}>Netbanking</Text>
                                </Left>
                                <Right>
                                     <Icon name="ios-arrow-forward" type="Ionicons"/>
                                </Right>
                            </TouchableOpacity>
                        </CardItem>
                    </Card>
                    <Text/>
                    <Card style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                        <CardItem style={{backgroundColor:"#d9d9d9",borderRadius:responsiveWidth(5)}}>
                            <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5)}}>Others</Text>
                        </CardItem>
                        <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                            <TouchableOpacity  onPress={() => this.setState({'check':!this.state.check})} style={{flexDirection:"row"}}>
                                <Left>
                                    <Thumbnail square source={require("../../../../assets/icon/cash.png")} 
                                            style={{height:responsiveWidth(10),width:responsiveWidth(10)}}/>
                                    <Text style={{fontWeight:"400",marginLeft:responsiveWidth(3)}}>Cash</Text>
                                </Left>
                                <Right>
                                <CheckBox
                                    containerStyle={{backgroundColor: '#f2f2f2',
                                    borderColor: '#f2f2f2',}}
                                    checkedIcon='check'
                                    iconType='material'
                                    uncheckedIcon='check'
                                    checked={this.state.check}
                                />
                                </Right>
                            </TouchableOpacity>
                        </CardItem>
                    </Card>
                </Content>
            </LinearGradient>
    </Container>
)}
}
