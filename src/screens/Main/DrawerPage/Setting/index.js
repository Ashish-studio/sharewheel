
import React from 'react';
import {View, Text, StyleSheet, Button, TouchableOpacity,Switch,Picker,Slider,AsyncStorage} from "react-native";
import { Container, Header, Content,Toast} from 'native-base';
import HeaderCOM from '../../../../components/Header';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import { LinearGradient } from 'expo-linear-gradient';
import Firebase from "../../../../config/firebase/firebase";


export default class Setting extends React.Component {
    constructor(props){
        super(props);
        this.state={
            switchValue:false,
            PickerSelectedVal : ''
            
        }        
    }
   
    getSelectedPickerValue=()=>{
        Alert.alert("Selected country is : " +this.state.PickerSelectedVal);
      }

  toggleSwitch = (valueLoc,valueStroage,valueNotification) => {
   
    this.setState({ 
        switchValueLocation: valueLoc,
        switchValueStroge: valueStroage,
        switchValueNotification: valueNotification
     });
    
  };
  signout = async () =>{
    const response = await Firebase.userSignOut();
    //console.log(response)
    if (response.status) {
        AsyncStorage.removeItem("UID");
        Toast.show({
          text:response.message,
          textStyle:{textAlign:"center"},
          duration:2000,
          style:{borderRadius:10,margin:responsiveWidth(10)}
        });
        this.props.navigation.navigate('auth');
      }
    // if (response){
    //     AsyncStorage.removeItem("Name");
    //     AsyncStorage.removeItem("Email");
    //     AsyncStorage.removeItem("Photo");

    //     this.props.navigation.navigate('auth');
    // }
    
  }
    render(){
        return (
            <Container>
                <HeaderCOM {...this.props} name="Settings"/>

            <Content>
            <View style={{marginTop:responsiveHeight(5),marginLeft:responsiveWidth(2),marginRight:responsiveWidth(2)}}>
            <Text style={{fontSize:responsiveHeight(2.5)}}>Location</Text>
            <Switch
            style={{marginTop:responsiveHeight(-3)}}
            onValueChange={this.toggleSwitch}
            value={this.state.switchValue}
            />
            </View>

            <View style={{marginTop:responsiveHeight(5),marginLeft:responsiveWidth(2),marginRight:responsiveWidth(2)}}>
            <Text style={{fontSize:responsiveHeight(2.5)}}>Storage</Text>
            <Switch
            style={{marginTop:responsiveHeight(-3)}}
            onValueChange={this.toggleSwitch}
            value={this.state.switchValue}
            />
            </View>

            <View style={{marginTop:responsiveHeight(5),marginLeft:responsiveWidth(2),marginRight:responsiveWidth(2)}}>
            <Text style={{fontSize:responsiveHeight(2.5)}}>Alert Notification</Text>
            <Switch
            style={{marginTop:responsiveHeight(-3)}}
            onValueChange={this.toggleSwitch}
            value={this.state.switchValue}
            />
            </View>

            <View style={{marginTop:responsiveHeight(5),marginLeft:responsiveWidth(2),marginRight:responsiveWidth(2)}}>
            <Text style={{fontSize:responsiveHeight(2.5)}}>Theme</Text>
            <Picker
            style={{marginLeft:responsiveWidth(70),marginTop:responsiveHeight(-5)}}
            selectedValue={this.state.PickerSelectedVal}
            onValueChange={(itemValue, itemIndex) => this.setState({PickerSelectedVal: itemValue})} >

            <Picker.Item label="Light" value="Light"  />
            <Picker.Item label="Dark" value="Dark" />

            </Picker>
            </View>

            <View style={{marginTop:responsiveHeight(5),marginLeft:responsiveWidth(2),marginRight:responsiveWidth(2)}}>
            <Text style={{fontSize:responsiveHeight(2.5)}}>Country</Text>
            <Picker
            style={{marginLeft:responsiveWidth(70),marginTop:responsiveHeight(-5)}}
            selectedValue={this.state.PickerSelectedVal}
            onValueChange={(itemValue, itemIndex) => this.setState({PickerSelectedVal: itemValue})} >

           <Picker.Item label="India" value="India" />
           <Picker.Item label="USA" value="USA" />
           <Picker.Item label="China" value="China" />
           <Picker.Item label="Russia" value="Russia" />
           <Picker.Item label="United Kingdom" value="United Kingdom" />
           <Picker.Item label="France" value="France" />

            </Picker>
            </View>

            <View>
            <Text style={{marginTop:responsiveHeight(5),marginLeft:responsiveWidth(2),marginRight:responsiveWidth(2),fontSize:responsiveHeight(2.5)}}>Brightness</Text>
            <Slider
            style={{width:responsiveWidth(80), height:responsiveWidth(10),marginLeft:responsiveWidth(10)}}
            minimumValue={1}
            maximumValue={100}
            minimumTrackTintColor="#00000"
            maximumTrackTintColor="#00000"
            />
            </View>

            <TouchableOpacity onPress={()=> this.props.navigation.navigate('Profile')}
                          style={{margin:responsiveWidth(5),marginTop:responsiveHeight(5),alignItems:"center"}}>
            <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#1CB5E0', '#000046']}
                                  style={{borderRadius: 5,height:responsiveHeight(7),width:responsiveWidth(90)}}>
                <View >
                <Text style={{color:'white',textAlign:"center",marginTop:responsiveHeight(2)}}>Account Settings</Text>
                </View>
            </LinearGradient>
            </TouchableOpacity>
        
            <TouchableOpacity onPress={()=>this.signout()}
                          style={{margin:responsiveWidth(5),marginTop:responsiveHeight(1),alignItems:"center"}}>
            <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#1CB5E0', '#000046']}
                                  style={{borderRadius: 5,height:responsiveHeight(7),width:responsiveWidth(90)}}>
                <View >
                <Text style={{color:'white',textAlign:"center",marginTop:responsiveHeight(2)}}>Log Out</Text>
                </View>
            </LinearGradient>
            </TouchableOpacity>

            <View>
                <Text style={{marginTop:responsiveHeight(2),marginLeft:responsiveWidth(42)}}>Version:1.0.0</Text>
            </View>

        </Content>
            </Container>
           
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:"center",
        justifyContent:"center"
    },
    headerText: {
        fontSize: 20,
        margin: 10,
        fontWeight: "bold"
      },
      menuContent: {
        color: "#000",
        fontWeight: "bold",
        padding: 2,
        fontSize: 20
      }
});