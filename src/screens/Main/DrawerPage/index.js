import About from './About';
import History from './History';
import Payment from "./Payments";
import ReferEarn from "./ReferEarn";
import Setting from "./Setting";
import Support from "./Support";

export {
    About,History,Payment,ReferEarn,Setting,Support
}