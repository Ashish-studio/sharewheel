
import React from 'react';
import {View, Text, StyleSheet, Button, TouchableOpacity} from "react-native";


export default class MyDetails extends React.Component {
    constructor(props){
        super(props);
    }
    render(){
        return (
            <View style={styles.container}>
                <Text >I am from my details Screen</Text>
            
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:"center",
        justifyContent:"center"
    }
});