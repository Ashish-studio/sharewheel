import React, { Component } from 'react';
import {Text,View,Image,ScrollView,AsyncStorage} from 'react-native';
import ParallaxScrollView from 'react-native-parallax-scrollview';
import { responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import { Button, Icon } from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Firebase from "../../../../config/firebase/firebase";
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';

export default class RNCollapsingToolbar extends Component {
  constructor(props){
    super(props);
    this.state={
      name:"",
      email:"",
      photo:"",
    }

}
componentDidMount =  async () => {
  var  user_Id=await AsyncStorage.getItem('UID');
  const uid = JSON.parse(user_Id);
  const response = await Firebase.getUser(uid);
  //console.log(response)
 this.setState({name:response.username,email:response.email,photo:response.photoUrl});
 this.getPermissionAsync();
} 

getPermissionAsync = async () => {
  if (Constants.platform.ios) {
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (status !== 'granted') {
      alert('Sorry, we need camera roll permissions to make this work!');
    }
  }
};

_pickImage = async () => {
  try {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4,3],
      quality: 1,
    });
    if (!result.cancelled) {
      this.setState({ photo: result.uri });
      var  user_Id=await AsyncStorage.getItem('UID');
      const uid = JSON.parse(user_Id);
      const picUpDate = await Firebase.userPicUpdate(uid,this.state.photo);
    }

    console.log(result);
  } catch (E) {
    console.log(E);
  }
};


  render() {
    return (
      <ParallaxScrollView
        windowHeight={responsiveHeight(45)}
        backgroundSource={{uri:this.state.photo}}
        navBarTitle={this.state.name}
        navBarTitleColor='#fff'
        navBarColor='#000046'
        headerView={(
                    <View style={{alignItems:"center",justifyContent:"center"}} >
                      <View style={{backgroundColor:"blue",height:responsiveHeight(15),width:responsiveHeight(15),
                                    borderRadius:responsiveHeight(15)}}>
                            <Image  source={{uri:this.state.photo}}
                                  style={{height:responsiveHeight(15),width:responsiveHeight(15),borderRadius:responsiveHeight(15)}}/>           
                        <View style={{alignItems:"flex-end",marginTop:responsiveHeight(-4)}}>
                          <TouchableOpacity onPress={()=>this._pickImage()}>
                            <Icon name="ios-add-circle" type="Ionicons" style={{color:"#000046"}}/>
                          </TouchableOpacity>
                        </View>
                      </View>
                      <View style={{alignItems:"center",justifyContent:"center"}}>
                        <Text style={{color:"#fff",fontSize:responsiveFontSize(3),margin:responsiveFontSize(1)}}>
                          {this.state.name}
                        </Text>
                        <Text style={{color:"#fff",fontSize:responsiveFontSize(2.5)}}>{this.state.email}</Text>
                      </View>
                    </View>
                  )}
        leftIcon={{name: 'menu', color: '#fff', size: responsiveFontSize(3)}}
        //leftIconOnPress={}
        rightIcon={{name: 'dots-three-vertical', color: '#fff', size: responsiveFontSize(3), type: 'entypo'}}
        //rightIconOnPress={}
      >
        <ScrollView style={{flex: 1, backgroundColor: 'rgba(228, 117, 125, 1)'}}>
          <View style={{height: 800, justifyContent: 'center', alignItems: 'center'}}>
            <Text style={{fontSize: 32, color: 'white'}}>Custom view</Text>
          </View>
         
        </ScrollView>
      </ParallaxScrollView>
    );
  }
}

