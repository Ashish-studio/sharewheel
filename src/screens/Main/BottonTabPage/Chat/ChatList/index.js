import React from 'react';
import {View, Text, StyleSheet, Button, TouchableOpacity,Alert} from "react-native";
import { Container, Content, List, ListItem, Left, Body, Right, Thumbnail,Icon} from 'native-base';
import HeaderCOM from '../../../../../components/Header';


export default class ChatList extends React.Component {
    constructor(props){
        super(props);
    }
    render(){
       // console.log(this.props)
        return (
            <Container>
       <HeaderCOM {...this.props} 
                        name="Inbox" 
                        ltbtnValue={true} 
                        rtfBtnValue={true} 
                        ltbtnTitle="menu" 
                        rtbtnTitle="dots-three-vertical" 
                        onActionLtBtn = {()=>{this.props.navigation.navigate("DrawerOpen")}}/>
        <Content>
        <List>
            <ListItem avatar>
              <Left>
                <Thumbnail source={require('../../../../../assets/profileUser.jpg')} />
              </Left>
              <Body>
              <TouchableOpacity onPress={()=>this.props.navigation.navigate('chatlistcontent')}>
                <Text style={{fontWeight:"bold"}}>Kumar Pratik</Text>
                <Text note>Doing what you like will always keep you happy . .</Text>
                </TouchableOpacity>
              </Body>
              <Right style={{justifyContent:"center"}}>
              <TouchableOpacity onPress={()=>this.props.navigation.navigate('chatlistcontent')}>
              <Icon name="right" type="AntDesign" />
              </TouchableOpacity>
              </Right>
            </ListItem>
            <ListItem avatar>
              <Left>
                <Thumbnail source={require('../../../../../assets/profileUser.jpg')} />
              </Left>
              <Body>
              <TouchableOpacity onPress={()=>this.props.navigation.navigate('chatlistcontent')}>
                <Text style={{fontWeight:"bold"}}>Kumar Pratik</Text>
                <Text note>Doing what you like will always keep you happy . .</Text>
                </TouchableOpacity>
              </Body>
              <Right  style={{justifyContent:"center"}}>
              <TouchableOpacity onPress={()=>this.props.navigation.navigate("chatlistcontent")}>
              <Icon name="right" type="AntDesign" />
              </TouchableOpacity>
              </Right>
            </ListItem>
           
          </List>
        </Content>
        </Container>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:"center",
        justifyContent:"center"
    }
});