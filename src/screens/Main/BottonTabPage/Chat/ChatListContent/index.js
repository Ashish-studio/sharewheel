
import React from 'react';
import {View, Text, StyleSheet, Button, TouchableOpacity} from "react-native";
import { Container, Content, List, ListItem, Left, Body, Right, Thumbnail,Icon} from 'native-base';
import { GiftedChat } from 'react-native-gifted-chat';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import HeaderCOM from '../../../../../components/Header';


export default class ChatListContent extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            messages: ["hello"],
          }
    }
    componentDidMount() {
        this.setState({
          messages: [
            {
              _id: 1,
              text: 'Hello developer',
              createdAt: new Date(),
              user: {
                _id: 2,
                name: 'React Native',
                avatar: 'https://placeimg.com/140/140/any',
              },
            },
          ],
        })
      }
     
      onSend(messages = []) {
        this.setState(previousState => ({
          messages: GiftedChat.append(previousState.messages, messages),
        }))
      }
     
    render(){
        return (
            <Container>
              <View style={{flex:1}}>
                <HeaderCOM {...this.props} 
                        name="Kumar Pratik" 
                        ltbtnValue={true} 
                        rtfBtnValue={true} 
                        ltbtnTitle="arrow-back" 
                        rtbtnTitle="dots-three-vertical" 
                        onActionLtBtn = {()=>{this.props.navigation.goBack()}}
                        option = {"View Contact","Search","Search","Report","Block"}/>
                <GiftedChat
                  messages={this.state.messages}
                  onSend={messages => this.onSend(messages)}
                  user={{
                    _id: 1,
                  }}
                />
                <KeyboardSpacer/>
              </View>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:"center",
        justifyContent:"center"
    }
});