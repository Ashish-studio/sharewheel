
import React from 'react';
import {View, Text,StyleSheet,TouchableOpacity,ImageBackground,Alert,Button,Image} from "react-native";
import { Container,Content,Icon} from 'native-base';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import { LinearGradient } from 'expo-linear-gradient';
import BottomDrawer from 'rn-bottom-drawer';
import Display from 'react-native-display';

export default class HOMESCREEN extends React.Component {
    constructor(props){
        super(props);
        this.state={
            enable:false
        }
    }
    
render(){
 return (
    <Container>
        <ImageBackground source={require('../../../../assets/home.png')} resizeMode="stretch" style={{flex:1}}>
            <Content>
                <View style={{flexDirection:"row"}}>
                    <View style={{flex:1,marginLeft:responsiveWidth(3)}}>
                        <TouchableOpacity onPress={()=> {}}>
                            <Icon name="menu"   style={{color:"#fff"}}/>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:1,alignItems:"flex-end",marginRight:responsiveWidth(3)}}>
                        <TouchableOpacity onPress={()=>Alert.alert("notification")}>
                            <Icon name="bell" type="FontAwesome" style={{color:"#fff"}}/>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{marginTop:responsiveHeight(10),justifyContent:"center",alignItems:"center"}}>
                    <Text style={{color:'#fff',fontSize:responsiveFontSize(4),margin:responsiveWidth(5),
                                fontWeight:"bold"}}>SHAREWHEELZ</Text>
                    <Text  style={{color:'#fff',fontSize:responsiveFontSize(3),marginLeft:responsiveWidth(5),
                                fontWeight:"bold"}}>
                        Find Your Perfect Pool
                    </Text>
                    <Text style={{color:'#fff',fontSize:responsiveFontSize(3),margin:responsiveWidth(5),textAlign:"center"}}>
                        The Most Cost Effective Method of Travel At Your Place
                    </Text> 
                </View>
                
            </Content>
            <BottomDrawer
                containerHeight={responsiveHeight(55)}
                startUp={false}
                onExpanded={()=>this.setState({enable:true})}
                onCollapsed={()=>this.setState({enable:false})}
            >
                <LinearGradient start={{x: 0, y: 0}} end={{x: 0, y: 1}} colors={['#000046','#1CB5E0']} 
                            style={{flex:1,}}>
                    
                    <Display enable={!this.state.enable} 
                            exit="fadeOutLeft"
                            enter="fadeInLeft">
                        <View style={{alignItems:"center",justifyContent:"center",margin:responsiveWidth(2),marginBottom:0}}>
                            <Text style={{color:"#fff",fontSize:responsiveFontSize(2.5)}}>Get or Find your pool</Text>
                        </View>
                        <View style={{alignItems:"center",justifyContent:"center",margin:responsiveWidth(2),flexDirection:"row"}}>
                            <View style={{alignItems:"center",flexDirection:"row",marginRight:responsiveWidth(10)}}>
                            <Icon name="ios-search" style={{color:"#fff"}}/>
                                <Text style={{color:"#fff",fontSize:responsiveFontSize(2),marginLeft:responsiveWidth(2)}}>Get Pool</Text>
                            </View>
                            <View style={{alignItems:"center",flexDirection:"row",marginLeft:responsiveWidth(10)}}>
                            <Icon name="plus-circle-outline" type="MaterialCommunityIcons" style={{color:"#fff"}}/>
                                <Text style={{color:"#fff",fontSize:responsiveFontSize(2),marginLeft:responsiveWidth(2)}}>Offer Pool</Text>
                            </View>
                        </View>
                    </Display>
                    <Display enable={this.state.enable} 
                            enterDuration={500} 
                            exitDuration={250}
                            exit="fadeOutDown"
                            enter="fadeInUp" >
                        <View style={{alignItems:"center",justifyContent:"center",margin:responsiveWidth(5),marginBottom:0}}>
                            <Text style={{color:"#fff",fontSize:responsiveFontSize(3)}}>Get or Find your pool</Text>
                        </View>
                    </Display>
                    <Display enable={this.state.enable} 
                            enterDuration={500} 
                            exitDuration={250}
                            exit="fadeOutLeft"
                            enter="fadeInLeft">
                        <TouchableOpacity onPress={()=> this.props.navigation.navigate("offerride")}>
                            <View style={{alignItems:"center",margin:responsiveWidth(5),marginTop:responsiveHeight(8),flexDirection:"row"}}>
                                <Image source={require("../../../../assets/findride.png")} 
                                    style={{width:responsiveWidth(50),height:responsiveHeight(10)}}/>
                                <Text style={{color:"#fff",fontSize:responsiveFontSize(2.5),marginLeft:responsiveWidth(5)}}>Get Your Pool</Text>
                            </View>
                        </TouchableOpacity>
                    </Display>
                    <Display enable={this.state.enable} 
                            enterDuration={500} 
                            exitDuration={250}
                            exit="fadeOutRight"
                            enter="fadeInRight">
                        <TouchableOpacity onPress={()=> this.props.navigation.navigate("offerride")}>
                            <View style={{alignItems:"center",justifyContent:"flex-end",margin:responsiveWidth(5),flexDirection:"row"}}>
                                <Text style={{color:"#fff",fontSize:responsiveFontSize(2.5),marginRight:responsiveWidth(5)}}>Offer Your Pool</Text>
                                <Image source={require("../../../../assets/offerride.png")} 
                                    style={{width:responsiveWidth(50),height:responsiveHeight(10)}}/>
                            </View>
                        </TouchableOpacity>
                    </Display>
                </LinearGradient>
            </BottomDrawer>
        </ImageBackground>
    </Container>
);}
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:"center",
        justifyContent:"center"
    },
});