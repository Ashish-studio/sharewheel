import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Body,  Icon,Item,Input,Button,Textarea} from 'native-base';
import { TextInput ,View ,Text,StyleSheet,TouchableOpacity,Alert} from 'react-native';
import {responsiveHeight,responsiveWidth, responsiveFontSize} from'react-native-responsive-dimensions'
import HeaderCOM from '../../../../components/Header';
import { LinearGradient } from 'expo-linear-gradient';
import DatePicker from 'react-native-datepicker';
import Modal from "../../../../components/Mapmodal";


export default class OfferRide extends React.Component {
    constructor(props){
        super(props);
        this.state = { chosenDate: new Date(),
                        time : new Date(),
                        showModal:false,
                        showModal1:false,
                        pickupval:"",
                        destval:"",
                        countPeople:0,
                        seatIconClr:[],
                        
                        prefIconClr1:"#000",prefIconClr2:"#000",prefIconClr3:"#000",
                        fontWeight1:"normal", fontWeight2:"normal", fontWeight3:"normal"
                              };
    }
    prefferencesClrHandle = (i) => {
      if(i==1){ this.setState({prefIconClr1: this.state.prefIconClr1 === '#000' ? 'red' : '#000',
                              fontWeight1: this.state.fontWeight1 === 'normal' ? 'bold' : 'normal'})}
      if(i==2){ this.setState({prefIconClr2: this.state.prefIconClr2 === '#000' ? 'red' : '#000',
                              fontWeight2: this.state.fontWeight2 === 'normal' ? 'bold' : 'normal'})}
      if(i==3){ this.setState({prefIconClr3: this.state.prefIconClr3 === '#000' ? 'red' : '#000',
                              fontWeight3: this.state.fontWeight3 === 'normal' ? 'bold' : 'normal'})}
    }
    setval = (pickup,show) => {
      this.setState({pickupval:pickup,showModal:!show})
    } 
    setvaldest = (dest,show) => {
     this.setState({destval:dest,showModal1:!show})
    }
    seatIconClrHandle = (k) => {
      this.setState({countPeople:k})
      var newclr = [];
      for(var i =1;i<=6;i++){
        if(i<=k){
          newclr.push('green')
        }
        else{
          newclr.push('black')
        }
      }
      this.setState({seatIconClr:newclr})
      //console.log(this.state.seatIconClr);
    }
   
render(){
     // console.log(this.state.color1)
  return (
          
            <Container>
               <View style={{flex:1}}>
              <LinearGradient start={{x: 0, y: 0}} end={{x: 0, y: 1}} colors={['#1CB5E0', '#000046']}
                                  style={{height:responsiveHeight(100),width:responsiveWidth(100)}}>
            <HeaderCOM {...this.props} 
                        name="Offer a pool" 
                        ltbtnValue={true} 
                        rtfBtnValue={true} 
                        ltbtnTitle="menu" 
                        rtbtnTitle="dots-three-vertical" 
                        onActionLtBtn = {()=>{this.props.navigation.navigate("DrawerOpen")}}/>
            
            <Content padder>
          <Card style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
          <CardItem header nobordered style={{backgroundColor:"#d9d9d9",borderRadius:responsiveWidth(5)}}>
              <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5)}}>Select a Location</Text>
            </CardItem>
            <CardItem bordered style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                <Body style={{alignItems:"center"}}>
                    <Item rounded >
                        <Icon active name='map-marker-alt'  type="FontAwesome5" style={{color:"green"}}/>
                        <Input placeholder="Leaving From" 
                              value={this.state.pickupval}
                              style={{width:responsiveWidth(80)}}
                              onTouchStart={()=>this.setState({showModal:true})}
                        />
                    </Item>
                    <Text/>
                    <Item rounded >
                        <Icon active name='map-marker-alt'  type="FontAwesome5" style={{color:"red"}}/>
                        <Input placeholder="Going To" style={{width:responsiveWidth(80),}}
                              value={this.state.destval}
                              onTouchStart={()=>this.setState({showModal1:true})}/>
                    </Item>
                    <Text/>
                </Body>
            </CardItem>
          </Card>
          <Text/>
          <Card style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
            <CardItem header nobordered style={{backgroundColor:"#d9d9d9",borderRadius:responsiveWidth(5)}}>
              <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5)}}>Select Date & Time</Text>
            </CardItem>
            <CardItem nobordered style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5),marginTop:responsiveHeight(2),
                                        marginBottom:responsiveHeight(2)}}>
                
              <DatePicker
                date={this.state.chosenDate} 
                mode="date" 
                format="DD-MM-YYYY"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                    dateIcon: {
                      position: 'absolute',
                      left: 0,
                      top: 4,
                      marginLeft: 0
                    },
                    dateInput: {
                      marginLeft: 36
                    }
                  }}
                onDateChange={(date) => {this.setState({chosenDate: date})}}
            />
            <Icon active name='clock'  type="FontAwesome5" style={{marginLeft:responsiveWidth(5),color:"#ff5c33"}}/> 
           <DatePicker 
          date={this.state.time} 
          mode="time" 
          format="hh:mm A"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          iconComponent={<Icon/>}
          onDateChange={(time) => {this.setState({time: time})}}
        />
            </CardItem>
          </Card>
          <Text/>
          <Card style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
          <CardItem header nobordered style={{backgroundColor:"#d9d9d9",borderRadius:responsiveWidth(5)}}>
              <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5)}}>How Many Seats ?</Text>
            </CardItem>
            <CardItem bordered style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
              <TouchableOpacity onPress={()=> this.seatIconClrHandle(1)}>
                  <Icon active name="user-alt"type="FontAwesome5" style={{color:this.state.seatIconClr[0]}}/>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> this.seatIconClrHandle(2)}>
                  <Icon active name="user-alt"type="FontAwesome5" style={{color:this.state.seatIconClr[1]}}/>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> this.seatIconClrHandle(3)}>
                  <Icon active name="user-alt"type="FontAwesome5" style={{color:this.state.seatIconClr[2]}}/>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> this.seatIconClrHandle(4)}>
                  <Icon active name="user-alt"type="FontAwesome5" style={{color:this.state.seatIconClr[3]}}/>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> this.seatIconClrHandle(5)}>
                  <Icon active name="user-alt"type="FontAwesome5" style={{color:this.state.seatIconClr[4]}}/>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> this.seatIconClrHandle(6)}>
                  <Icon active name="user-alt"type="FontAwesome5" style={{color:this.state.seatIconClr[5]}}/>
              </TouchableOpacity>
              </CardItem>
              <CardItem header nobordered style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2)}}>{this.state.countPeople} Seats</Text>
              </CardItem>
            </Card>
            <Text/>
            <Card style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
          <CardItem header nobordered style={{backgroundColor:"#d9d9d9",borderRadius:responsiveWidth(5)}}>
              <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5)}}>Prefferences</Text>
            </CardItem>
            <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5),alignItems:"center"}}>
              <TouchableOpacity onPress={()=>{this.prefferencesClrHandle(1)} } style={{flexDirection:"row",alignItems:"center",
                                                          justifyContent:"center",margin:responsiveWidth(2)}}>
              <Icon name="paw-off" type="MaterialCommunityIcons" style={{color:this.state.prefIconClr1}}/>
              <Text style={{fontSize:responsiveFontSize(2),fontWeight:this.state.fontWeight1}}>No pets</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=>{this.prefferencesClrHandle(2)} } style={{flexDirection:"row",alignItems:"center",
                                                          justifyContent:"center",margin:responsiveWidth(2),marginLeft:responsiveWidth(15)}}>
               <Icon name="smoking-off" type="MaterialCommunityIcons" style={{color:this.state.prefIconClr2}}/>
              <Text style={{fontSize:responsiveFontSize(2),fontWeight:this.state.fontWeight2}}>No Smoking</Text>
              </TouchableOpacity>
            </CardItem>
            <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5),alignItems:"center"}}>
              <TouchableOpacity onPress={()=>{this.prefferencesClrHandle(3)} } style={{flexDirection:"row",alignItems:"center",
                                                          justifyContent:"center",margin:responsiveWidth(2)}}>
              <Icon name="volume-strike" type="Foundation" style={{color:this.state.prefIconClr3}}/>
              <Text style={{fontSize:responsiveFontSize(2),fontWeight:this.state.fontWeight3}}>No Loud music</Text>
              </TouchableOpacity>
              
              </CardItem>
              
            </Card>
            <Text/>
            <Card style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
              <CardItem header nobordered style={{backgroundColor:"#d9d9d9",borderRadius:responsiveWidth(5)}}>
                  <Text  style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5)}}>Description</Text>
                </CardItem>
                <CardItem header nobordered style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                <Textarea style={{width:responsiveWidth(80),height:responsiveHeight(20),padding:responsiveWidth(2),
                    margin:responsiveWidth(3)}} bordered placeholder="Textarea" />
               </CardItem> 
               </Card>
              <View style={{alignItems:"center",justifyContent:"center"}}>
                  <TouchableOpacity onPress={()=> Alert.alert("Ride Offered Successfully")}
                                  style={{margin:responsiveWidth(5),width:responsiveWidth(50)}}>
                    <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#1CB5E0', '#000046']}
                                  style={{borderRadius: responsiveWidth(5),height:responsiveHeight(8),width:responsiveWidth(50),
                                        alignItems:"center",justifyContent:"center",marginBottom:responsiveHeight(5)}}>
                      <Text style={{color:'white'}}>Offer Ride</Text>
                    </LinearGradient>
                  </TouchableOpacity>
              </View>
         
          </Content>
          {this.state.showModal && 
            <Modal
                                onRef = {ref => this.Modal = ref}
                            
                                onAction = {(pick,show) => this.setval(pick,show)}
                            />
            }
            {this.state.showModal1 && 
            <Modal
                                onRef = {ref => this.Modal = ref}
                                okbtn = {true}
                                cnlbtn = {true}
                                onAction = {(dest,show) => this.setvaldest(dest,show)}
                            />
            }
        </LinearGradient>
      </View>
    </Container>
            
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:"center",
        justifyContent:"center"
    }
});