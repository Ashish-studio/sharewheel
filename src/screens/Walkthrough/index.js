import React from 'react'; 

import { StyleSheet, View, Text } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

import AppIntroSlider from 'react-native-app-intro-slider';
import { responsiveWidth, responsiveFontSize ,responsiveHeight} from 'react-native-responsive-dimensions';


export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showRealApp: false,
      
    };
  }
  _onDone = () => {
    
    this.props.navigation.navigate("auth");
    //this.setState({ showRealApp: true });
  };
  _onSkip = () => {
    
    this.props.navigation.navigate("auth");
  };
  render() {
      return (
        <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} 
                  colors={["#1CB5E0","#000046"]}
                  style={{flex:1}}>
          <AppIntroSlider
            slides={slides}
            onDone={this._onDone}
            showSkipButton={true}
            onSkip={this._onSkip}
          />
        </LinearGradient>
      );
  }
}

const slides = [
  {
    key: 's1',
    title: 'Welcome To ShareWheelz',
    text: 'ShareWheelz is a carpooling service that helps you find, connect others going your way.',
    titleStyle: {color:"#fff",fontSize:responsiveFontSize(3.5)},
    textStyle: {color:"#fff",fontSize:responsiveFontSize(2.5)},
    image: require('../../assets/homeimg.png'),
    imageStyle: {height:responsiveWidth(30),width:responsiveWidth(70)},
  },
  {
    key: 's2',
    title: 'Verified Drivers',
    text: 'Verified drivers, who might be your colleagues and neighbours, offer rides during commute.Riders who are going the same way at the same time.',
    titleStyle: {color:"#fff",fontSize:responsiveFontSize(3.5)},
    textStyle: {color:"#fff",fontSize:responsiveFontSize(2.5)},
    image: require('../../assets/vdriver.png'),
    imageStyle: {height:responsiveHeight(30),width:responsiveWidth(50),borderRadius:responsiveWidth(5)}
  },
  
  {
    key: 's3',
    title: "Saving Money & Gasoline ",
    text: 'ShareWheelz promise to help you save money on gas and reduce traffic and help in heaaling the earth. ',
    titleStyle: {color:"#fff",fontSize:responsiveFontSize(3.5)},
    textStyle: {color:"#fff",fontSize:responsiveFontSize(2.5)},
    image: require('../../assets/savefuel.png'),
    imageStyle: {height:responsiveHeight(30),width:responsiveWidth(80),borderRadius:responsiveWidth(5)}
  },
  {
    key: 's4',
    title: "Refer and earn points",
    text: 'Invite your friends to join Sharewheelz and get 100 points for each friend that joins using your referal code and your friend also gets 100 points on Sharewheelz membership. ',
    titleStyle: {color:"#fff",fontSize:responsiveFontSize(3.5)},
    textStyle: {color:"#fff",fontSize:responsiveFontSize(2.5)},
    image: require('../../assets/refer.png'),
    imageStyle: {width:responsiveWidth(70),height:responsiveHeight(30),borderRadius:responsiveWidth(5)},
  },

];