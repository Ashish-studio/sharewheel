import React, { Component } from 'react';
import { View, Text, Image,AsyncStorage ,StyleSheet,ActivityIndicator} from 'react-native';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import { LinearGradient } from 'expo-linear-gradient';

class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  retrieveItem = async (key) => {
    return new Promise(async (resolve, reject) => {
        try {
            const retrievedItem = await AsyncStorage.getItem(key);
            const item = JSON.parse(retrievedItem);
            resolve(item);
        } catch (error) {
            reject(error.message);
        }
    })
  }

  componentDidMount = () => {
    setTimeout(()=>{
      this.retrieveItem("UID").then((response)=>{
        if(!response){
          this.props.navigation.navigate("walk");
        } else{
          this.props.navigation.navigate("main");
        }
      }).catch((error) =>{
        alert(error)
      });
    },2000);
  } 
  
  render() {
    return (
      <View style={styles.container}>
        <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#1CB5E0', '#000046']}
                        style={{height:responsiveHeight(100),width:responsiveWidth(100)}}>
          <View style={{alignItems:"center",marginTop:responsiveHeight(40)}}>
            <Image source={require("../../assets/splash.png")}
                style={{width:responsiveWidth(20),height:responsiveWidth(20)}} />
          </View>
          <View style={{alignItems:"center",marginTop:responsiveHeight(2)}}>
            <Image source={{uri:"https://www.animatedimages.org/data/media/67/animated-car-image-0147.gif"}}
                style={{width:responsiveWidth(30),height:responsiveWidth(15),}} />
          </View>
           
        </LinearGradient>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    
  },
  loader : {
    position:"absolute",
    bottom:100,
    alignSelf : "center",
  }

})

export default Splash;