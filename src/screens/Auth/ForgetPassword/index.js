import React from "react";
import {View,Text,TouchableOpacity,Image, ImageBackground,TextInput, Alert} from "react-native";
import {Container, Header, Content, Card, CardItem, Body,Input, Button,Toast,Left,Icon} from "native-base";
import {responsiveHeight,responsiveWidth,responsiveFontSize} from "react-native-responsive-dimensions";
import { LinearGradient } from "expo-linear-gradient";
import HeaderCOM from '../../../components/Header';
import {SafeAreaView} from "react-native-safe-area-context";
import Firebase from "../../../config/firebase/firebase";

class Forgot extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      email : "Enter your Registered Email",
      loading : false,
      color:"#000"
    };
  }

  validateEmail = ()=>{
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    if(emailPattern.test(this.state.email)){
        return true;
    }
    return false;
  }
  
  resetPassword = () =>{
    let email = this.state.email;
    if(!this.validateEmail()){
      this.setState({color:"red"});
      Toast.show({
        text:"please fill email in correct format (abc@email.com)",
        textStyle:{textAlign:"center"},
        duration:2000,
        style:{borderRadius:10,margin:responsiveWidth(10)}
      });
      return false;
    }
    this.setState({loading:true});
    Firebase.passwordReset(email)
    .then((response)=>{
      //console.log(response)
      if(response.status){
      this.setState({color:"green"});
      Toast.show({
        text:response.message,
        textStyle:{textAlign:"center"},
        duration:2000,
        style:{borderRadius:10,margin:responsiveWidth(10)}
        });
        this.setState({loading:false});
      }
      else{
        this.setState({color:"red"});
        Toast.show({
          text:response.message,
          textStyle:{textAlign:"center"},
          duration:2000,
          style:{borderRadius:10,margin:responsiveWidth(10)}
        });
      }
    })
    .catch((err)=>{
      this.setState({loading:false})
      Toast.show({
        text:err,
        textStyle:{textAlign:"center"},
        duration:2000,
        style:{borderRadius:10,margin:responsiveWidth(10)}
      });
    })
  } 
  render(){
    return(
      <Container>
        <Header style={{backgroundColor:"#000046"}} >
          <Body style={{flexDirection:"row",alignItems:"center"}}>
          <TouchableOpacity onPress={()=> this.props.navigation.goBack()}>
              <Icon name="arrow-back" style={{color:"#fff",fontSize:responsiveFontSize(4)}}/>
            </TouchableOpacity>
            <Text style={{color:"#fff",fontSize:responsiveFontSize(2.5),marginLeft:responsiveWidth(5)}}>Trouble Logging In?</Text>
          </Body>
        </Header>
        <Content padder>
          <Card style={{backgroundColor:"#fff",borderRadius:responsiveWidth(5)}}>
            <CardItem header bordered style={{backgroundColor:"#1CB5E0",borderRadius:responsiveWidth(5)}}>
              <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5)}}>Forgot Password ?</Text>
            </CardItem>
            <CardItem >
              <Body style={{alignItems:"center",justifyContent:"center"}}>
              
                <Text style={{fontSize:responsiveFontSize(2.3)}}>Please enter the email you signed up with.We will send you a link to change your password.</Text>
                <Input  style={{borderWidth:1,width:responsiveWidth(80),height:responsiveHeight(6),
                                margin:responsiveHeight(5),borderRadius:responsiveWidth(2),borderColor:this.state.color}}
                        onChangeText={(email) => this.setState({email})}
                        placeholder="Enter your Registered Email">

                </Input>

              </Body>
            </CardItem>
            <CardItem footer bordered style={{borderRadius:responsiveWidth(5)}}>
            <Body style={{alignItems:"center",justifyContent:"center"}}>
            <TouchableOpacity onPress={() => this.resetPassword()} >
                  <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={[!this.state.loading?"#1CB5E0":"grey", !this.state.loading?"#000046":"grey"]}
                                  style={{borderRadius:responsiveWidth(5),height:responsiveHeight(7),
                                        width:responsiveWidth(60),margin:responsiveWidth(2)}}>
                    <View >
                    <Text style={{color:'white',textAlign:"center",marginTop:responsiveHeight(2)}}>
                    {this.state.loading?"Please Wait...":"Send Password Reset Link"}
                    </Text>
                    </View>
                  </LinearGradient>
                </TouchableOpacity>
                </Body>
           </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}


export default Forgot;