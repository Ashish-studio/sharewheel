import React from 'react';
import {Text, View,TouchableOpacity,AsyncStorage,TouchableWithoutFeedback,Keyboard,ImageBackground,Image} from 'react-native';
import {SocialIcon} from 'react-native-elements';
import {Container,Body,Icon,Input, Content,Item, Toast,Label,Card,CardItem} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import { LinearGradient } from 'expo-linear-gradient';
import Firebase from "../../../config/firebase/firebase";
import KeyboardSpacer from 'react-native-keyboard-spacer';
import Spinner from 'react-native-loading-spinner-overlay';

export default class Login extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      email: '',
      password: '',
      color1:"grey",
      color2:"grey",
      passwordVisibility: true,
      passwordShow:'ios-eye',
      loading:false,
      showLoader:false,

    };
  }
  handlePasswordVisibility = () => {
    this.setState({
      passwordShow: this.state.passwordShow === 'ios-eye' ? 'ios-eye-off' : 'ios-eye',
      passwordVisibility: !this.state.passwordVisibility
    })
  }
  validateEmail = ()=>{
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    if(emailPattern.test(this.state.email)){
        return true;
    }
    return false;
  }
  
  validatePass = ()=>{
    var passPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*_-])[A-Za-z\d!@#$%^&*_-]{8,}$/;
    if(passPattern.test(this.state.password)){
        return true;
    }
    return false;
  }

  handleOnLogin = async () => {
  if(this.state.email.length > 0 && this.state.password.length > 0)
    {
      if(!this.validateEmail())
        {
          this.setState({color1:'red',color2:"grey",loading:false})
          Toast.show({
                text:"please fill email in correct format (abc@email.com)",
                textStyle:{textAlign:"center"},
                duration:2000,
                style:{borderRadius:10,margin:responsiveWidth(10)}
              });
        }
      else if(!this.validatePass())
        {
          this.setState({color1:"green",color2:'red',loading:false})
          Toast.show({
            text:"Password must include minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character",
            textStyle:{textAlign:"center"},
            duration:2000,
            style:{borderRadius:10,margin:responsiveWidth(10)}
          }); 
        }
      else
        { this.setState({color1:"grey",color2:"grey",loading:true})
          try 
            {  this.setState({showLoader:true})
              const response = await Firebase.loginWithEmail(this.state.email, this.state.password)
              console.log(response)
              if (response.status) 
                { this.setState({showLoader:false,loading:false});
                  Toast.show({
                    text:response.message,
                    textStyle:{textAlign:"center"},
                    duration:2000,
                    style:{borderRadius:10,margin:responsiveWidth(10)}
                  });
                  this.props.navigation.navigate('main');
                  this.storeItem("UID",response.data.user.uid);
                }
              else
                { this.setState({showLoader:false})
                  this.setState({color1:"red",color2:"red",loading:false})
                  Toast.show({
                    text:"Incorrect Username or Password!",
                    textStyle:{textAlign:"center"},
                    duration:2000,
                    style:{borderRadius:10,margin:responsiveWidth(10)}
                  }); 
                }
              this.setState({loading:false});
            } 
          catch (error) 
            { Toast.show({
                  text:response.message,
                  textStyle:{textAlign:"center"},
                  duration:2000,
                  style:{borderRadius:10,margin:responsiveWidth(10)}
                });  
            }
        } 
  }
  else 
    { Toast.show({
        text:"Fill the requried fields (*)",
        textStyle:{textAlign:"center"},
        duration:2000,
        style:{borderRadius:10,margin:responsiveWidth(10)}
      }); 
      this.setState({color1:'red',color2:"red"});
    }
}
facebookLogin = async () =>{
  try
    { this.setState({showLoader:true})
      const response = await Firebase.loginWithFacebook()
      //console.log(response.facebookUserData.user.uid);
      if (response) {
          this.setState({showLoader:false})
          Toast.show({
            text:"Login Successfull",
            textStyle:{textAlign:"center"},
            duration:2000,
            style:{borderRadius:10,margin:responsiveWidth(10)}
          });
         this.props.navigation.navigate('main');
         this.storeItem("UID",response.facebookUserData.user.uid);
          this.setState({loading:false});
        } 
  }
  catch(error){
    this.setState({showLoader:false})
    Toast.show({
      text:"Something went wrong!",
      textStyle:{textAlign:"center"},
      duration:2000,
      style:{borderRadius:10,margin:responsiveWidth(10)}
    });
  }
}
googleLogin = async () =>{
  try
    { this.setState({showLoader:true})
      const response = await  Firebase.loginWithGoogle()
      console.log(response);
      // if (response) {
      //     this.setState({showLoader:false})
      //     Toast.show({
      //       text:"Login Successfull",
      //       textStyle:{textAlign:"center"},
      //       duration:2000,
      //       style:{borderRadius:10,margin:responsiveWidth(10)}
      //     });
      //     this.props.navigation.navigate('main');
          // this.storeItem("Name",response.facebookUserData.additionalUserInfo.profile.name);
          // this.storeItem("Email",response.facebookUserData.additionalUserInfo.profile.email);
          // this.storeItem("Photo",response.facebookUserData.additionalUserInfo.profile.picture.data.url);
         // this.setState({loading:false});
       // } 
  }
  catch(error){
    this.setState({showLoader:false})
    Toast.show({
      text:"Something went wrong!",
      textStyle:{textAlign:"center"},
      duration:2000,
      style:{borderRadius:10,margin:responsiveWidth(10)}
    });
  }
  // if (response) {
   
  //   Toast.show({
  //     text:"Login Successfull",
  //     textStyle:{textAlign:"center"},
  //     duration:2000,
  //     style:{borderRadius:10,margin:responsiveWidth(10)}
  //   });
  //   this.storeItem("Name",response.facebookUserData.additionalUserInfo.profile.name);
  //   this.storeItem("Email",response.facebookUserData.additionalUserInfo.profile.email);
  //   this.storeItem("Photo",response.facebookUserData.additionalUserInfo.profile.picture.data.url);
  //   this.setState({loading:false});
    
  //   this.props.navigation.navigate('main');
  // } 
}
  storeItem = async (key, item) => {
    return new Promise(async (resolve, reject) => {
      try {
        var jsonOfItem = await AsyncStorage.setItem(key, JSON.stringify(item));
        resolve(jsonOfItem);
      } catch (error) {
        reject(error);
      }
    })
  } 

 render(){
 return ( 
      <View style={{flex:1}} >
        <Container >
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <ImageBackground source={{uri : 'https://i.imgur.com/YYT12jf.png'}} resizeMode="stretch" 
                                style={{width:responsiveWidth(100),height:responsiveHeight(100)}}>
                  <Content>
                    <View style={{margin:responsiveWidth(5),marginBottom:responsiveHeight(0)}}>
                      <View style={{flexDirection:'row',marginTop:responsiveHeight(2),justifyContent:'center',alignItems:'center'}}>
                        <Image source={require("../../../assets/splash.png")} 
                              style={{height:responsiveWidth(20),width:responsiveWidth(20),
                                  borderRadius:responsiveWidth(5)}}/>
                        <Text style={{color:'#fff',fontSize:responsiveFontSize(4),fontWeight:"bold",marginLeft:responsiveWidth(2)}}>
                          SHAREWHEELZ
                        </Text>
                   
                      </View>
                       </View>
                    <View style={{marginTop:responsiveHeight(3),marginLeft:responsiveWidth(7)}}>
                      <Text style={{color:'#fff',fontWeight:"bold",fontSize:responsiveFontSize(3)}}>
                        Welcome Back,</Text>
                      <Text style={{color:'#fff',fontSize:responsiveFontSize(3)}}>
                        Sign In to continue.
                      </Text>
                    </View>
                    <View style={{margin:responsiveWidth(5),marginTop:responsiveHeight(0)}}>
                      <Card style={{backgroundColor:"rgba(255,255,255,0.8)",borderRadius:responsiveWidth(5)}}>
                        <CardItem style={{backgroundColor:"transparent",borderRadius:responsiveWidth(5)}}>
                          <View style={{flex:1,height:responsiveHeight(7)}}>
                            <Item style={{borderColor:this.state.color1}} floatingLabel>
                              <Label >Email</Label>
                              <Input value={this.state.email}
                                      onChangeText={(email) => this.setState({email})}
                                      autoCapitalize = "none"
                                />
                            </Item>
                          </View>
                          <View style={{height:responsiveHeight(7),justifyContent:"flex-end"}}>
                          <Icon name="ios-mail" style={{color:"#000",fontSize:responsiveFontSize(4),marginLeft:responsiveWidth(1)}}/>
                          </View>
                        </CardItem>
                        <CardItem style={{backgroundColor:"transparent",borderRadius:responsiveWidth(5)}}>
                          <View style={{flex:1,height:responsiveHeight(7)}}>
                            <Item style={{borderColor:this.state.color2}} floatingLabel>
                              <Label >Password</Label>
                              <Input value={this.state.password}
                                      onChangeText={(password)=>this.setState({password})}
                                      secureTextEntry={this.state.passwordVisibility}
                                />
                            </Item>
                          </View>
                          <View style={{justifyContent:"flex-end",height:responsiveHeight(7)}}>
                            <TouchableOpacity onPress={this.handlePasswordVisibility}>
                              <Icon name={this.state.passwordShow} style={{color:"#000",marginLeft:responsiveWidth(1),fontSize:responsiveFontSize(4)}}/>
                            </TouchableOpacity>
                          </View>
                        </CardItem>
                        <CardItem style={{backgroundColor:"transparent",borderRadius:responsiveWidth(5),justifyContent:"flex-end"}}>
                          <TouchableOpacity onPress={()=> this.props.navigation.navigate('forgot')}>
                            <Text style={{fontSize:responsiveFontSize(2)}}>
                              Forget Password?
                            </Text>
                          </TouchableOpacity>
                        </CardItem>
                        <CardItem style={{backgroundColor:"transparent",borderRadius:responsiveWidth(5),justifyContent:"center"}}>
                          <TouchableOpacity onPress={() => this.handleOnLogin()}
                              style={{alignItems:"center",justifyContent:"center",width:responsiveWidth(50)}}>
                            <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} 
                                            colors={[!this.state.loading?"#1CB5E0":"grey", !this.state.loading?"#000046":"grey"]}
                                            style={{borderRadius: 5,height:responsiveHeight(7),width:responsiveWidth(50),
                                                  alignItems:"center",justifyContent:"center"}}>
                              <View >
                              <Text style={{color:'white'}}>
                                {this.state.loading?"Please Wait...":"Login"}</Text>
                              </View>
                            </LinearGradient>
                          </TouchableOpacity>
                        
                        </CardItem>
                        
                        <CardItem style={{backgroundColor:"transparent",borderRadius:responsiveWidth(5),justifyContent:"center"}}>
                          <Text style={{fontSize:responsiveFontSize(2)}}>
                          ---------------OR---------------
                          </Text>
                        </CardItem>
                        <CardItem style={{backgroundColor:"transparent",borderRadius:responsiveWidth(5),justifyContent:"center"}}>
                        
                          <TouchableOpacity onPress={() => this.facebookLogin()}
                                          style={{alignItems:"center",justifyContent:"center"}}> 
                                  <SocialIcon type="facebook"/>
                          </TouchableOpacity>
                          <TouchableOpacity onPress={() => this.googleLogin()}
                                          style={{alignItems:"center",justifyContent:"center"}}> 
                                  <SocialIcon type="google" />
                          </TouchableOpacity>
                          
                        </CardItem>
                        <CardItem style={{backgroundColor:"transparent",borderRadius:responsiveWidth(5),justifyContent:"center"}}>
                          <TouchableOpacity onPress={()=> this.props.navigation.navigate('signup')}>
                            <Text style={{fontSize:responsiveFontSize(2)}}>
                              Don't have an account? Sign Up
                            </Text>
                          </TouchableOpacity>
                        </CardItem>
                      </Card> 
                    </View>
                  </Content>
                  <KeyboardSpacer/>
                </ImageBackground>
            </TouchableWithoutFeedback>
          </Container>
           <Spinner animation="fade"
                    visible={this.state.showLoader}
                    textContent={'Loading...'}
                    textStyle={{color:"#fff"}}
                  />
        </View>
 )}
}

   
 

