import React from 'react';
import {Text, View,TouchableOpacity,AsyncStorage,TouchableWithoutFeedback,Keyboard,KeyboardAvoidingView,ImageBackground,Image} from 'react-native';
import {SocialIcon,CheckBox,Button,} from 'react-native-elements';
import {Container,Body,Icon,Input, Content,Item, Toast,Label,Card,CardItem} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import { LinearGradient } from 'expo-linear-gradient';
import Firebase from "../../../config/firebase/firebase";
import KeyboardSpacer from 'react-native-keyboard-spacer';
import Spinner from 'react-native-loading-spinner-overlay';

export default class SignUp extends React.Component {
    constructor(props){
      super(props);
      this.state = {
                    username: '',
                    email:'',
                    password: '',
                    conPassword:'',
                    color:"grey",
                    color1:"grey",
                    color2:"grey",
                    color3:"grey",
                    loading: false,
                    passwordVisibility: true,
                    confirmPasswordVisibility:true,
                    passwordShow:'ios-eye',
                    conPasswordShow:'ios-eye',
                    check:true,
                    showLoader:false
                  };
      }
  handlePasswordVisibility = () => {
    this.setState({
      passwordShow: this.state.passwordShow === 'ios-eye' ? 'ios-eye-off' : 'ios-eye',
      passwordVisibility: !this.state.passwordVisibility
    })
  }
  handleConfirmPasswordVisibility = () => {
    this.setState({
      conPasswordShow: this.state.conPasswordShow === 'ios-eye' ? 'ios-eye-off' : 'ios-eye',
      confirmPasswordVisibility: !this.state.confirmPasswordVisibility
    })
  }
  validateEmail = ()=>{
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    if(emailPattern.test(this.state.email)){
        return true;
      }
    return false;
  }
  validatePass = ()=>{
    var passPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*_-])[A-Za-z\d!@#$%^&*_-]{8,}$/;
    if(passPattern.test(this.state.password)){
        return true;
      }
    return false;
  }
  handleOnSingup = async () => {
    const { username,email,password,conPassword } = this.state
    if( username == "" || email == "" || password == "" || conPassword == "" )
      {
        this.setState({color:"red",color1:"red",color2:'red',color3:"red",loading:false})
        Toast.show({
              text:"Please fill the required fields",
              textStyle:{textAlign:"center"},
              duration:2000,
              style:{borderRadius:10,margin:responsiveWidth(10)}
            });
      }
    else
      {
        if(!this.validateEmail())
          {
            this.setState({color:"green",color1:'red',loading:false})
            Toast.show({
                  text:"please fill email in correct format (abc@email.com)",
                  textStyle:{textAlign:"center"},
                  duration:2000,
                  style:{borderRadius:10,margin:responsiveWidth(10)}
                });
          }
        else if(!this.validatePass())
          {
            this.setState({color:"green",color1:"green",color2:'red',loading:false})
            Toast.show({
              text:"Password must include minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character",
              textStyle:{textAlign:"center"},
              duration:2000,
              style:{borderRadius:10,margin:responsiveWidth(10)}
            }); 
          }
        else if(password !== conPassword)
          {
            this.setState({color:"green",color1:"green",color2:'red',color3:"red",loading:false})
            Toast.show({
              text:"Password must include be same",
              textStyle:{textAlign:"center"},
              duration:2000,
              style:{borderRadius:10,margin:responsiveWidth(10)}
            }); 
          }
        else
          { this.setState({color:"grey",color1:"grey",color2:"grey",color:"grey",loading:true})
            try { this.setState({showLoader:true})
                  const response = await Firebase.signupWithEmail(username,email,password)
                 //console.log(response);
                  this.setState({showLoader:false})
                  this.props.navigation.navigate("login")
                  this.setState({loading:false})
                 
                } 
            catch (error) {this.setState({loading:false,showLoader:false})}
          }
      }
  }
storeItem = async (key,Item) => {
    return new Promise(async (resolve, reject) => {
      try {
        var jsonOfItem = await AsyncStorage.setItem(key, JSON.stringify(Item));
        resolve(jsonOfItem);
      } catch (error) {
        reject(error);
      }
    })
  } 
render(){
  return (
          <View style={{flex:1}} >
             <Spinner animation="fade"
                    visible={this.state.showLoader}
                    textContent={'Loading...'}
                    textStyle={{color:"#fff"}}
                  />
          <Container >
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
              
                <ImageBackground source={{uri : 'https://i.imgur.com/YYT12jf.png'}} resizeMode="stretch" 
                style={{width:responsiveWidth(100),height:responsiveHeight(100)}}>
                <Content>
                  <View style={{margin:responsiveWidth(5),marginBottom:responsiveHeight(0)}}>
                  <View style={{flexDirection:'row',marginTop:responsiveHeight(5),justifyContent:'center',alignItems:'center'}}>
                        <Image source={require("../../../assets/splash.png")} 
                              style={{height:responsiveWidth(20),width:responsiveWidth(20),
                                  borderRadius:responsiveWidth(5)}}/>
                        <Text style={{color:'#fff',fontSize:responsiveFontSize(4),fontWeight:"bold",marginLeft:responsiveWidth(2)}}>
                          SHAREWHEELZ
                        </Text>
                   
                      </View>
                    
                  </View>
                  <View style={{marginTop:responsiveHeight(3),marginLeft:responsiveWidth(7)}}>
                    <Text style={{color:'#fff',fontWeight:"bold",fontSize:responsiveFontSize(3)}}>
                      Welcome ,</Text>
                    <Text style={{color:'#fff',fontSize:responsiveFontSize(3)}}>
                      Sign Up to continue.
                    </Text>
                  </View>
                  <View style={{margin:responsiveWidth(5),marginTop:responsiveHeight(0)}}>
                    <Card style={{backgroundColor:"rgba(255,255,255,0.8)",borderRadius:responsiveWidth(5)}}>
                      <CardItem style={{backgroundColor:"transparent",borderRadius:responsiveWidth(5)}}>
                        <View style={{flex:1,height:responsiveHeight(7)}}>
                          <Item style={{borderColor:this.state.color}} floatingLabel>
                            <Label >Username</Label>
                            <Input value={this.state.username}
                                    onChangeText={(username) => this.setState({username})}
                                    autoCapitalize = "none"
                              />
                          </Item>
                        </View>
                        <View style={{alignItems:"flex-end",justifyContent:"flex-end",height:responsiveHeight(7)}}>
                            <Icon name="user" type="Entypo" style={{fontSize:responsiveFontSize(4),marginLeft:responsiveWidth(1)}} />
                        </View>
                      </CardItem>
                      <CardItem style={{backgroundColor:"transparent",borderRadius:responsiveWidth(5)}}>
                        <View style={{flex:1,height:responsiveHeight(7)}}>
                          <Item style={{borderColor:this.state.color1}} floatingLabel>
                            <Label >Email</Label>
                            <Input value={this.state.email}
                                    onChangeText={(email) => this.setState({email})}
                                    autoCapitalize = "none"
                              />
                          </Item>
                        </View>
                        <View style={{alignItems:"flex-end",justifyContent:"flex-end",height:responsiveHeight(7)}}>
                          <Icon name="ios-mail" style={{fontSize:responsiveFontSize(4),marginLeft:responsiveWidth(1)}}/>
                        </View>
                      </CardItem>
                      <CardItem style={{backgroundColor:"transparent",borderRadius:responsiveWidth(5)}}>
                        <View style={{flex:1,height:responsiveHeight(7)}}>
                          <Item style={{borderColor:this.state.color2}} floatingLabel>
                            <Label >Password</Label>
                            <Input value={this.state.password}
                                    onChangeText={(password)=>this.setState({password})}
                                    secureTextEntry={this.state.passwordVisibility}
                              />
                          </Item>
                        </View>
                        <View style={{justifyContent:"flex-end",height:responsiveHeight(7)}}>
                          <TouchableOpacity onPress={this.handlePasswordVisibility}>
                            <Icon name={this.state.passwordShow} style={{fontSize:responsiveFontSize(4),marginLeft:responsiveWidth(1)}}/>
                          </TouchableOpacity>
                        </View>
                      </CardItem>
                      <CardItem style={{backgroundColor:"transparent",borderRadius:responsiveWidth(5)}}>
                        <View style={{flex:1,height:responsiveHeight(7)}}>
                          <Item style={{borderColor:this.state.color3}} floatingLabel>
                            <Label >Confirm Password</Label>
                            <Input value={this.state.conPassword}
                                    onChangeText={(conPassword)=>this.setState({conPassword})}
                                    secureTextEntry={this.state.confirmPasswordVisibility}
                              />
                          </Item>
                        </View>
                        <View style={{justifyContent:"flex-end",height:responsiveHeight(7)}}>
                          <TouchableOpacity onPress={this.handleConfirmPasswordVisibility}>
                            <Icon name={this.state.conPasswordShow} style={{fontSize:responsiveFontSize(4),marginLeft:responsiveWidth(1)}}/>
                          </TouchableOpacity>
                        </View>
                      </CardItem>
                      
                       <CheckBox
                        containerStyle={{backgroundColor: 'transparent',
                        borderColor: 'transparent'}}
                        checkedIcon='check-box'
                        iconType='material'
                        uncheckedIcon='check-box-outline-blank'
                        title='Agree to terms and conditions'
                        checkedTitle='You agreed to our terms and conditions'
                        checked={this.state.check}
                        onPress={() => this.setState({'check':!this.state.check})}
                      />
                      <CardItem style={{backgroundColor:"transparent",borderRadius:responsiveWidth(5),justifyContent:"center"}}>
                        <TouchableOpacity onPress={() => this.handleOnSingup()}
                            style={{alignItems:"center",justifyContent:"center",width:responsiveWidth(50)}}>
                          <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} 
                                          colors={[!this.state.loading?"#1CB5E0":"grey", !this.state.loading?"#000046":"grey"]}
                                          style={{borderRadius: 5,height:responsiveHeight(7),width:responsiveWidth(50),
                                                alignItems:"center",justifyContent:"center"}}>
                            <View >
                            <Text style={{color:'white'}}>
                              {this.state.loading?"Please Wait...":"Sign Up"}</Text>
                            </View>
                          </LinearGradient>
                        </TouchableOpacity>
                      </CardItem>
                      <CardItem style={{backgroundColor:"transparent",borderRadius:responsiveWidth(5),justifyContent:"center"}}>
                        <TouchableOpacity onPress={()=> this.props.navigation.navigate('login')}>
                          <Text style={{fontSize:responsiveFontSize(2)}}>
                             Already have an account? Sign In
                          </Text>
                        </TouchableOpacity>
                      </CardItem>
                    </Card>
                  </View>  
                  </Content> 
                  <KeyboardSpacer/>    
                </ImageBackground>
            </TouchableWithoutFeedback>
          </Container>
          </View>
        );
      }
    }

   
 

