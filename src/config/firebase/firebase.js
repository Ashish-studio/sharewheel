import * as firebase from 'firebase';
import firebaseConfig from './config';
import * as Facebook from 'expo-facebook';
import * as Google from 'expo-google-app-auth';


if (firebase.apps.length === 0) {
    firebase.initializeApp(firebaseConfig);
}
else{
    firebase.app();
}   
var userdata;
const Firebase = {
    loginWithEmail: (email, password) => {
        let response = {}
        return new Promise(async(resolve,reject)=>{
            try{
                let user_data = await firebase.auth().signInWithEmailAndPassword(email, password)
                response.status = true;
                response.message = "Login Success";
                response.data = user_data;
                resolve(response)
            }
            catch(err){
                response.status = false;
                response.message = err.message;
                response.data = undefined;
                resolve(response)
            }
        })
    },
    
    signupWithEmail: (username,email, password) => {
        let response = {}
        return new Promise(async(resolve,reject)=>{
            try{
                let user_data = await firebase.auth().createUserWithEmailAndPassword(email, password).then((user)=>{
                    
                     if (firebase.auth().currentUser) {
                     const userId = firebase.auth().currentUser.uid;
                     const userPhotoUrl = "https://i.ya-webdesign.com/images/profile-avatar-png-4.png";
                     const provider = "ShareWheelz";
                      if (userId) {
                          firebase.database().ref('users/' + userId).set({
                            uid:userId,
                            username:username,
                            email:email,
                            photoUrl:userPhotoUrl,
                            provider:provider
                          })
                      }
                    }
                  }).catch(function(error) {
                    // Handle Errors here.
                      var errorCode = error.code;
                      var errorMessage = error.message;
                  })
                response.status = true;
                response.message = "Account Created Successfully";
                response.data = user_data;
                resolve(response)
            }
            catch(err){
                return (err)
            }
        })
    },
    passwordReset : (email) => {
        let response = {}
        return new Promise(async(resolve,reject)=>{
            try{
                let result = await firebase.auth().sendPasswordResetEmail(email)
                response.status = true;
                response.message = "Password Reset Link has been sent to your Registered Email Address.";
                response.data = result;
                resolve(response) 
            }
            catch(err){
                response.status = false;
                response.message = err.message;
                resolve(response)
            }
        })
    },
    loginWithFacebook : async () => {
       try{ 
           await Facebook.initializeAsync('223957445346702');
            const permissions = ['public_profile', 'email'];
            const {type,token} = await Facebook.logInWithReadPermissionsAsync({permissions});
            switch (type) {
                    case 'success': {
                                    await firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL); 
                                    const credential = firebase.auth.FacebookAuthProvider.credential(token);
                                    const facebookUserData = await firebase.auth().signInWithCredential(credential);
                                    //console.log(facebookUserData)
                                    const response1 = await fetch(`https://graph.facebook.com/me?access_token=${token}&fields=id,name,picture.width(600).height(600)`);
                                    const userInfo = await response1.json();
                                    console.log(userInfo)
                                    if (facebookUserData.user.uid) {
                                        const userId = facebookUserData.user.uid;
                                        const userName = facebookUserData.user.displayName;
                                        const userEmail = facebookUserData.user.email;
                                        const userPhotoUrl = userInfo.picture.data.url;
                                        const provider = "Facebook";
                                          if (userId) {
                                              firebase.database().ref('users/' + userId).set({
                                                uid:userId,
                                                username:userName,
                                                email:userEmail,
                                                photoUrl:userPhotoUrl,
                                                provider:provider
                                              })
                                          }
                                       }
                                    return Promise.resolve({type: 'success',facebookUserData});
                                }
                    case 'cancel': {
                                     return Promise.reject({type: 'cancel'});
                                 }
                        }
        }
       catch(error){
                     return { error };
        }
      },
    loginWithGoogle : async () => {
        try{ 
            
                const permissions = {
                                    //expoClientId: '372959820165-l7356s4qgvb2l4demljdea06hvg93bof.apps.googleusercontent.com',
                                    androidClientId: '372959820165-fhhiosmn7gr4ife8860rmkuqba4t5i68.apps.googleusercontent.com',
                                   // iosClientId: '372959820165-vte712dl6d1li5iajqiekl8c35dk0rnv.apps.googleusercontent.com',
                                    scopes: ['profile', 'email']
                                };
                const {type,token} = await Google.logInAsync({permissions});
                console.log(type)
                switch (type) {
                        case 'success': {
                                        await firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL); 
                                        const credential = firebase.auth.GoogleAuthProvider.credential(token);
                                        const googleUserData = await firebase.auth().signInWithCredential(credential);   
                                        return Promise.resolve({type: 'success',googleUserData});
                                    }
                        case 'cancel': {
                                        return Promise.reject({type: 'cancel'});
                                    }
                            }
            }
        catch(error){
                        return { error };
            }
    }, 
    getUser : (userId) => {
        try{ firebase.database().ref('users/' + userId).on('value',(snapshot) =>{
             userdata = snapshot.val();
          });
          return (userdata)
         }
        catch(error){
                      return { error };
         }
        
    },
    userPicUpdate : (userId,userPic) => {
        try{  firebase.database().ref('users/' + userId).update({
            photoUrl:userPic
          })
         }
        catch(error){
                      return { error };
         }
        
    },
    userSignOut :  () => {
            let response = {}
            return new Promise(async(resolve,reject)=>{
                try{
                    let result = await firebase.auth().signOut()
                    response.status = true;
                    response.message = "logout success";
                    resolve(response) 
                }
                catch(err){
                    response.status = false;
                    response.message = err.message;
                    reject(response)
                }
            })
            
    },
      
      

}

export default Firebase