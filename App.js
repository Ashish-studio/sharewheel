import React from 'react';
import {Root} from "native-base";
import { SafeAreaProvider } from 'react-native-safe-area-context';
import AppContainer from "./src/routes/index";

class App extends React.Component {
  render(){
    return (
      <Root>
        <SafeAreaProvider>
          <AppContainer />
        </SafeAreaProvider>
      </Root>
    );
  }
}
export default App;
